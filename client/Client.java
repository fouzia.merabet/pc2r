package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.Socket;



public class Client {
	protected static final int PORT = 1235;
	protected static final int SIZE = 2000;

	// fonction qui parse la commande
/*	public static String parse_commande(String commande) {

		String[] sousCommandes = commande.split("/");

		switch (sousCommandes[0]) {
		case "WELCOME":
			return "Connexion reussi phase score coord";

		case "DENIED":
			return "Connexion refusé ";
		case "NEWPLAYER":
			return "Connexion refusé ";
		
		case "PLAYERLEFT":
			return "Connexion refusé ";
		
		case "SESSION":
			return "Connexion refusé ";
		
		case "WINNER":
			return "Connexion refusé ";
		
		case "TICK":
			return "Connexion refusé ";
		
		case "NEWOBJ":
			return "Connexion refusé ";
		
		
		default:
			return "Commande non prise en compte";

		}
	}*/

	public static void main(String[] args) {

		Socket socket = null;
		try {

			
			// Creation d'une socket
			socket = new Socket(InetAddress.getByName("127.0.0.1"), PORT);
			// Connexion au serveur
			BufferedReader canalLecture = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			PrintStream canalEcriture = new PrintStream(socket.getOutputStream());
			System.out.println("connexion etablie : " + socket.getPort());
			


			Thread thread_out = new Thread() {
				public void run() {
					
					char c;
					String line = "";
					
					while (true) {
						
						line = "";
						try {
							while ((c = (char) System.in.read()) != '\n') {
									line += c;
							}
							canalEcriture.println(line);
							System.out.println("Client -> Serveur = "+ line);
							canalEcriture.flush();
						
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				
				}
				
			};
			
			Thread thread_in = new Thread() {
				public void run() {
					while(true) {
						String line = "";
						try {
							while((line = canalLecture.readLine()) !=null){
								//line = canalLecture.readLine();
								//String res = parse_commande(line);
	
								System.out.println("Serveur -> Client = " + line);
							}
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			};
			
			thread_out.start();
			thread_in.start();

			try {
				thread_out.join();
				thread_in.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		
		} catch (IOException e) {
			System.err.println(e);
		} finally {
			try {
				if (socket != null)
					socket.close();
			} catch (IOException e2) {
			}
		}
	}
}
