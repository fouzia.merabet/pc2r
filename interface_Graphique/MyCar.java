package interface_Graphique;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.Scanner;

import javax.swing.JPanel;

public class MyCar extends JPanel {

	Graphics g;
	Scanner sc;
	public boolean jouer = true;

	public int refresh_tickrate = 50;
	public final double turnit = 0.599;
	public final double thrustit = 1; 
	public double angle; // angle teta 
	public double vx; // vitesse
	public double vy; // de la vaoiture

	Objectif objectif = new Objectif(0, 0, Color.BLACK);
	Car mycar;

	public MyCar(Graphics g, int angle) {
		this.g = g;
		this.angle = angle;
		mycar = new Car(50, 50, Color.BLACK);
		sc = new Scanner(System.in);
		this.creationObjectif();

	}

	public void jouer() {

		while ( true) {
			g.setColor(Color.LIGHT_GRAY);
			g.fillRect(0, 0, Fenetre.largeurFen, Fenetre.hauteurFen);
			//creationObjectif();
			dessineObjectif();
			dessineCar();

			sleep(1/refresh_tickrate);

			move();

		}
	}

	public void sleep(int time) {

		try {
			Thread.sleep(time);
		} catch (InterruptedException ex) {
			Thread.currentThread().interrupt();
		}
	}

	public void creationObjectif() {
		int randX, randY;

		int widthFen = ((Fenetre.largeurFen - 20) / 10) - 2;
		int heightFen = ((Fenetre.hauteurFen - 20) / 10) - 2;

		randX = (int) ((Math.random() * (widthFen)) + 3);
		randY = (int) ((Math.random() * (heightFen)) + 3);

		randX = randX * 10;
		randY = randY * 10;

		objectif = new Objectif(randX, randY, Color.GREEN);

	}


	public void dessineObjectif() {
		g.setColor(objectif.couleur);
		g.fillOval(objectif.posX, objectif.posY, 10, 10);
	}

	public void dessineCar() {

		g.setColor(mycar.couleur);
		g.fillOval(mycar.posX% Fenetre.hauteurFen, mycar.posY% Fenetre.largeurFen, 30, 30);
		showScore();

	}

	public void showScore() {
		g.setFont(new Font("TimesRoman", Font.PLAIN, 15));
		g.drawString(Integer.toString(0), 10, Fenetre.hauteurFen - 10);
	}

	/**
	 * tourner à droite
	 */
	public void turnRight() {
		this.angle = this.angle + turnit;
	}

	/**
	 * tourner à gauche
	 */
	public void turnLeft() {
		this.angle = this.angle - turnit;
	}
	/**
	 * Acceleration
	 */
	public void thrust() {
		this.vx = this.vx +thrustit* Math.cosh(this.angle);
		this.vy = this.vy +thrustit * Math.sin(this.angle);
	}
	
	/**
	 * se deplacer
	 */
	public void move() {
		mycar.posX += vx;
		mycar.posY += vy;
		
	}

	

}
