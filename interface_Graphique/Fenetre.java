package interface_Graphique;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Fenetre extends JFrame implements KeyListener {

	JPanel container = new JPanel();
	public static int largeurFen = 700;
	public static int hauteurFen = 700;

	MyCar bugato;

	JLabel gameOver = new JLabel();

	public Fenetre() {
		this.setTitle("CARS");
		this.setSize(largeurFen, hauteurFen);
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		container.setBackground(Color.LIGHT_GRAY);

		this.addKeyListener(this);
		this.setContentPane(container);
		this.setVisible(true);

		bugato = new MyCar(this.getGraphics(), 0);

	}

	public void startGame() {
		bugato.jouer();
		gameOver();
	}

	public void gameOver() {
		gameOver.setText("Game Over !");
		container.add(gameOver);
		container.repaint();
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		if (arg0.getKeyCode() == KeyEvent.VK_RIGHT )
			bugato.turnRight();
		if (arg0.getKeyCode() == KeyEvent.VK_LEFT )
			bugato.turnLeft();
		if (arg0.getKeyCode() == KeyEvent.VK_UP )
			bugato.thrust();

	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}
}
